package learning.g2g3.iamtheauthor;

import learning.g2g3.iamtheauthor.annotations.Author;
import learning.g2g3.iamtheauthor.annotations.Since;
import learning.g2g3.iamtheauthor.annotations.Version;


@Author("Foobar Qux and Qwerty U. Iop")
@Version("2.0")
public interface NorfAPIConnector {

    @Since("1.0")
    int getWibble();

    String getWobble();
}